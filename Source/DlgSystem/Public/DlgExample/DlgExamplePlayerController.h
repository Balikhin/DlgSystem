// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "DlgExamplePlayerController.generated.h"

UCLASS()
class ADlgExamplePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ADlgExamplePlayerController();

	UFUNCTION(BlueprintCallable, Category = DlgSystem)
	void StartDialogue(class UDlgDialogue* Dialogue, UObject* OtherParticipant);

	UFUNCTION(BlueprintCallable, Category = DlgSystem)
	void SelectDialogueOption(int32 Index);

protected:
	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface
protected:

	UPROPERTY(BlueprintReadOnly)
	class UDlgContext* ActiveContext = nullptr;

};


