// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "DlgExampleCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"

ADlgExampleCharacter::ADlgExampleCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 0.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 0.f;
	CameraBoom->RelativeRotation = FRotator(0.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ADlgExampleCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}


bool ADlgExampleCharacter::ModifyIntValue_Implementation(const FName& ValueName, bool bDelta, int32 Value)
{
	if (!DlgData.Integers.Contains(ValueName))
		DlgData.Integers.Add(ValueName, 0);

	if (bDelta)
		DlgData.Integers[ValueName] += Value;
	else
		DlgData.Integers[ValueName] = Value;

	return true;
}

bool ADlgExampleCharacter::ModifyFloatValue_Implementation(const FName& ValueName, bool bDelta, float Value)
{
	if (!DlgData.Floats.Contains(ValueName))
		DlgData.Floats.Add(ValueName, 0.0f);

	if (bDelta)
		DlgData.Floats[ValueName] += Value;
	else
		DlgData.Floats[ValueName] = Value;

	return true;
}

bool ADlgExampleCharacter::ModifyBoolValue_Implementation(const FName& ValueName, bool bValue)
{
	if (bValue)
		DlgData.TrueBools.Add(ValueName);
	else
		DlgData.TrueBools.Remove(ValueName);

	return true;
}

bool ADlgExampleCharacter::ModifyNameValue_Implementation(const FName& ValueName, const FName& NameValue)
{
	if (DlgData.Names.Contains(ValueName))
		DlgData.Names[ValueName] = NameValue;
	else
		DlgData.Names.Add(ValueName, NameValue);

	return true;
}


float ADlgExampleCharacter::GetFloatValue_Implementation(const FName& ValueName) const
{
	return DlgData.Floats.Contains(ValueName) ? DlgData.Floats[ValueName] : 0.0f;
}

int32 ADlgExampleCharacter::GetIntValue_Implementation(const FName& ValueName) const
{
	return DlgData.Integers.Contains(ValueName) ? DlgData.Integers[ValueName] : 0;
}

bool ADlgExampleCharacter::GetBoolValue_Implementation(const FName& ValueName) const
{
	return DlgData.TrueBools.Contains(ValueName);
}

FName ADlgExampleCharacter::GetNameValue_Implementation(const FName& ValueName) const
{
	return DlgData.Names.Contains(ValueName) ? DlgData.Names[ValueName] : NAME_None;
}
